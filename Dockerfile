FROM phusion/baseimage:0.9.16

CMD ["/sbin/my_init"]

RUN apt-get -qq update
RUN apt-get -y install wget psmisc mc daemontools curl

# Install Haproxy.
RUN \
  sed -i 's/^# \(.*-backports\s\)/\1/g' /etc/apt/sources.list && \
  apt-get update && \
  apt-get install -y haproxy=1.5.3-1~ubuntu14.04.1 && \
  sed -i 's/^ENABLED=.*/ENABLED=1/' /etc/default/haproxy && \
  rm -rf /var/lib/apt/lists/*

RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Enable SSH
RUN rm -f /etc/service/sshd/down
RUN /etc/my_init.d/00_regen_ssh_host_keys.sh

# Add files.
ADD haproxy.cfg /etc/haproxy/haproxy.cfg

RUN mkdir /etc/service/haproxy
ADD haproxy.sh /etc/service/haproxy/run

VOLUME /var/log/
VOLUME /etc/haproxy/
VOLUME /root/

EXPOSE 80
EXPOSE 443
